﻿using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public AudioSource source;

    void Start()
    {
        if (instance == null) instance = this;
        else {
            Destroy(gameObject);
            return;
        }


        DontDestroyOnLoad(gameObject);
    }
}
