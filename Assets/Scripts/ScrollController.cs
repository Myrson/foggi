﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScrollController : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    public Button scrollItemPrefab;

    // Start is called before the first frame update
    void Start()
    {
        List<string> cityList = LocalizationDataAPI.getCitiesList();
        foreach (var city in cityList)
        {
            generateItem(city);
        }

    }
    

    void generateItem(string name) {
        Button scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("Text").gameObject.GetComponent<Text>().text = name;
        scrollItemObj.onClick.AddListener(() => SetChoosenCity(name));
    }

    private void SetChoosenCity(String name)
    {
        LocalizationDataAPI.ChoosenCity = name;
        LocalizationDataAPI.AutoLocalization = false;
    }


}
