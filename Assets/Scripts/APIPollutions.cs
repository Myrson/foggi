﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using Microsoft.CSharp;
using UnityEngine;

namespace Assets.Scripts
{
    public class APIPollutions
    {
  


        private string _TOKEN_ID_ = "122e989544a02bbb46b46aa190733c724ee0b73f";
        public CityPollution CityPollution { get; set; }
        public string City { get; set; }
        public string Api_query { get; set; }

        public List<string> cityListy = new List<string>();
     


        public APIPollutions(String city)
        {
            City = city;
            Api_query = $"http://api.waqi.info/feed/{City}/?token={_TOKEN_ID_}";
            CityPollution = new CityPollution();
            string jsonString =  getApiData();
            ParseJsonToCityPollution(jsonString);
           
        }

        public APIPollutions(double lat, double longi, double lat2, double longi2) //new bounds
        {
            string x = lat.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            string y = longi.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            string x1 = lat2.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            string y1 = longi2.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);

            Api_query = $"https://api.waqi.info/map/bounds/?latlng=({x}),({y}),({x1}),({y1})&token={_TOKEN_ID_}";
            string jsonString = getApiData();
            ParseJsonToCitiesName(jsonString);
        }

        public APIPollutions(double lat, double lon )
        {
            Api_query = $"https://api.waqi.info/feed/geo:{lat};{lon}/?token={_TOKEN_ID_}";
            CityPollution = new CityPollution();
            string jsonString = getApiData();
            ParseJsonToCityPollution(jsonString);
        }

        private void ParseJsonToCitiesName(string jsonString)
        {
            dynamic jsonDe = JsonConvert.DeserializeObject(jsonString);
            if (jsonDe.status == "error")
            {
                Debug.Log("not found city");
             //  throw new Exception("not found city");
            }
            else
            {
                var data = jsonDe.data;
                foreach (var station in data)
                {
                    if (station.aqi != "-")
                    {
                        string cName = station.station.name;
                        cityListy.Add(cName);
                    }
                }
            }
        }

        private string getApiData()
        {
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(Api_query));

            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();


            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }

            return jsonString;
        }

        private void ParseJsonToCityPollution(string jsonString)
        {
            dynamic jsonDe = JsonConvert.DeserializeObject(jsonString);
            if (jsonDe.status == "error")
            {
                Debug.Log("not found city");
                CityPollution.CityName = LocalizationDataAPI.CityNameError;
            }
            else
            {
                CityPollution.CityName = jsonDe.data.city.name;
                CityPollution.Aqi = jsonDe.data.aqi;
                CityPollution.DominentPol = jsonDe.data.dominentpol;

                CityPollution.O3 = jsonDe.SelectToken("data.iaqi.o3.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.o3.v") : "-";
                CityPollution.No2 = jsonDe.SelectToken("data.iaqi.no2.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.no2.v") : "-";
                CityPollution.Pm10 = jsonDe.SelectToken("data.iaqi.pm10.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.pm10.v") : "-";
                CityPollution.Pm25 = jsonDe.SelectToken("data.iaqi.pm25.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.pm25.v") : "-";
                CityPollution.So2 = jsonDe.SelectToken("data.iaqi.so2.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.so2.v") : "-";
                CityPollution.Co = jsonDe.SelectToken("data.iaqi.co.v") != null ? ChangeToDecimal(jsonDe, "data.iaqi.co.v") : "-";
                CityPollution.GeoLat = jsonDe.SelectToken("data.city.geo[0]") != null ? ChangeToDecimal(jsonDe, "data.city.geo[0]") : "-";
                CityPollution.GeoLon= jsonDe.SelectToken("data.city.geo[1]") != null ? ChangeToDecimal(jsonDe, "data.city.geo[1]") : "-";

            }
        }

        private static String ChangeToDecimal(dynamic jsonDe, string json)
        {
            return Convert.ToDecimal(jsonDe.SelectToken(json)).ToString("0.0");
        }
    }

    //all places in lan long
    //https://aqicn.org/json-api/doc/#api-Map_Queries-GetMapStations
    //https://api.waqi.info/map/bounds/?latlng=36.991934,(-8.940194),41.872122,(-7.632074)&token=122e989544a02bbb46b46aa190733c724ee0b73f

    //https://api.waqi.info/map/bounds/?latlng=36.991934,(-8.940194),41.872122,(-7.632074)&token=122e989544a02bbb46b46aa190733c724ee0b73f

    //https://api.waqi.info/map/bounds/?latlng=38.459584,(-9.198899),41.819278,(-8.107403)&token=122e989544a02bbb46b46aa190733c724ee0b73f

    //https://api.waqi.info/map/bounds/?latlng=36.991934,(-8.940194),41.872122,(-7.632074)&token=122e989544a02bbb46b46aa190733c724ee0b73f
}
