﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class CityPollution
    {
        public string Aqi { get; set; }         //Real-time air quality infomrmation.
        public string Pm25 { get; set; }
        public string Pm10 { get; set; }
        public string O3 { get; set; }         //ozon
        public string No2 { get; set; }         //Nitrogen Dioxide
        public string Co { get; set; }          //Carbon Monoxyde
        public string So2 { get; set; }         //Sulphur Dioxide
        public string CityName { get; set; }
       
        public string DominentPol;             //dominant

        public string GeoLat { get; set; }
        public string GeoLon { get; set; }
    }
}
