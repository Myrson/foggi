﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Assets.SimpleAndroidNotifications;

public class APIManager : MonoBehaviour
{
    public TextMeshProUGUI ValueNumber;
    public TextMeshProUGUI ValueWord;
    public Image emojiImage;
    private  Emoji emoji;
    private  APIPollutions api;
    public Text cityNameInButton;
    private String cityName;
    private double longitudeBefore;
    private double latitudeBefore;
    public float NotificationRate = 3600.0f;
    private float nextNotification = 3600.0f;

    void Start()
    {
        cityName = LocalizationDataAPI.ChoosenCity;
        UpdateAirByCity();
    }

    private void UpdateAirByCity()
    {
        if (LocalizationDataAPI.AutoLocalization)
        {
            api = new APIPollutions(LocalizationDataAPI.LatitudeNow, LocalizationDataAPI.LongitudeNow);

            LocalizationDataAPI.ChoosenCity = api.CityPollution.CityName;

            latitudeBefore = LocalizationDataAPI.LatitudeNow;
            longitudeBefore = LocalizationDataAPI.LongitudeNow;
        }
        else
        {
            api = new APIPollutions(cityName);

            if (api.CityPollution.CityName == LocalizationDataAPI.CityNameError)
            {
                string[] cityNamesParts = cityName.Split(',');
                cityName = cityNamesParts[1];
                api = new APIPollutions(cityName);
                if (api.CityPollution.CityName != LocalizationDataAPI.CityNameError)
                {
                    cityName = LocalizationDataAPI.ChoosenCity;
                }
            }
        }

        if (api.CityPollution.CityName == LocalizationDataAPI.CityNameError) {
            cityNameInButton.text = api.CityPollution.CityName;
            cityName = LocalizationDataAPI.ChoosenCity;
        }
        else
        {
            if (Int32.Parse(api.CityPollution.Aqi) < EmojiGood.maxIQA)
            {
                emoji = new EmojiGood();
            }
            else if (Int32.Parse(api.CityPollution.Aqi) < EmojiUnhealthy.maxIQA)
            {
                emoji = new EmojiUnhealthy();
            }
            else
            {
                NotificationManager.Send(TimeSpan.FromSeconds(1), "warning!", " pollution levels are life threatening!", new Color(1, 0.3f, 0.15f), NotificationIcon.Dead);
                emoji = new EmojiDead();
            }

            ValueNumber.text = api.CityPollution.Aqi;
            ValueNumber.color = emoji.Color;

            ValueWord.text = emoji.TextIQA;
            ValueWord.color = emoji.Color;

            emojiImage.sprite = emoji.EmojiSprite;

            if (cityNameInButton != null) cityNameInButton.text = api.CityPollution.CityName;
        }
    }
    

    void Update()
    {
        if (cityName != LocalizationDataAPI.ChoosenCity)
        {
            cityName = LocalizationDataAPI.ChoosenCity;
            UpdateAirByCity();
            
        }


        if ( Time.time > nextNotification && Int32.Parse(api.CityPollution.Aqi) > EmojiDead.minIQA && Int32.Parse(api.CityPollution.Aqi) < EmojiDead.maxIQA)
        {
            NotificationManager.Send(TimeSpan.FromSeconds(1), "warning!", " pollution levels are life threatening!", new Color(1, 0.3f, 0.15f), NotificationIcon.Dead);
            nextNotification = Time.time + NotificationRate;
        }

    }

    public void setAutoLocalizationCity()
    {
        LocalizationDataAPI.AutoLocalization = true;

        LocalizationDataAPI.ChoosenCity = "";
        Update();
        //cityName = "";
    }
}
