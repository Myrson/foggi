﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class APIManagerAirQuality : MonoBehaviour
{
    private APIPollutions api;

    public TextMeshProUGUI Pm25;
    public TextMeshProUGUI Pm10;
    public TextMeshProUGUI O3;
    public TextMeshProUGUI No2;
    public TextMeshProUGUI Co;
    public TextMeshProUGUI So2;

    public String CityName;

    public TextMeshProUGUI DominentPol; 

    // Start is called before the first frame update
    void Start()
    {
        var city = LocalizationDataAPI.ChoosenCity;
        api = new APIPollutions(city);

        if (api.CityPollution.CityName == LocalizationDataAPI.CityNameError)
        {
            string[] cityNamesParts = city.Split(',');
            city = cityNamesParts[1];
            api = new APIPollutions(city);
            if (api.CityPollution.CityName != LocalizationDataAPI.CityNameError)
            {
                city = LocalizationDataAPI.ChoosenCity;
            }
        }

        Pm25.text = api.CityPollution.Pm25;
        Pm10.text = api.CityPollution.Pm10;
        O3.text = api.CityPollution.O3;
        No2.text = api.CityPollution.No2;
        Co.text = api.CityPollution.Co;
        So2.text = api.CityPollution.So2;
        DominentPol.text = api.CityPollution.DominentPol;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
