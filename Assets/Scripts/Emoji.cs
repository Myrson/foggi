﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public abstract class Emoji
    {

        public abstract int MinIQA { get;  }
        public abstract int MaxIQA { get; }
        public abstract string TextIQA { get; }
        public abstract Color32 Color { get; }
        public abstract Sprite EmojiSprite { get; }

    }

   public class EmojiGood :Emoji
    {
        public override int MinIQA { get => minIQA; }
        public override int MaxIQA { get => maxIQA; }
        public override string TextIQA { get => textIQA; }
        public override Color32 Color { get => color; }
        public override Sprite EmojiSprite { get => emojiSprite; }

        private const int minIQA = 0;
        public const int maxIQA = 150;
        private const String textIQA = "EXCELLENT";
        private Color32 color = new Color32(0x00, 0xB2, 0x5C, 0xFF);
        private Sprite emojiSprite = Resources.Load<Sprite>("EmojiGood");

        public EmojiGood()
        {
        }
    }

    public class EmojiUnhealthy : Emoji
    {
        public override int MinIQA { get => minIQA; }
        public override int MaxIQA { get => maxIQA; }
        public override string TextIQA { get => textIQA; }
        public override Color32 Color { get => color; }
        public override Sprite EmojiSprite { get => emojiSprite; }

        public const int minIQA = 150;
        public const int maxIQA = 300;
        public const String textIQA = "UNHEALTHY";
        public Color32 color = new Color32(0xFF, 0x67, 0x00, 0xFF);
        public Sprite emojiSprite= Resources.Load<Sprite>("EmojiUnhealthy");
        public EmojiUnhealthy()
        {
        }
    }

    public class EmojiDead : Emoji
    {
        public override int MinIQA { get => minIQA; }
        public override int MaxIQA { get => maxIQA; }
        public override string TextIQA { get => textIQA; }
        public override Color32 Color { get => color; }
        public override Sprite EmojiSprite { get => emojiSprite; }

        public const int minIQA = 300;
        public const int maxIQA = 500;
        public const String textIQA = "HAZARDOUS";
        public Color32 color = new Color32(0xB3, 0x3A, 0x3A, 0xFF);
        public Sprite emojiSprite = Resources.Load<Sprite>("EmojiDead");
        public EmojiDead()
        {
        }
    }
}
