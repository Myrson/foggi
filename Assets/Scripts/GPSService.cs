﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;

public class GPSService : MonoBehaviour
{
    public double lat; //52.229070 ;
    public double longi; //21.013123;
    void Start() {
        StartCoroutine(StartLocationService());
    }
    IEnumerator StartLocationService()
    {

        if (!Input.location.isEnabledByUser)
        {
            //jesli nie to daj powiadomienie ze jest wyloczny gps
            yield break;
        }
      

            // Start service before querying location
            Input.location.Start();

            // Wait until service initializes
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            // Service didn't initialize in 20 seconds
            if (maxWait < 1)
            {
                print("Timed out");
                yield break;
            }

            // Connection has failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                print("Unable to determine device location");
                yield break;
            }
            else
            {
                // Access granted and location value could be retrieved
                print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
                lat = Input.location.lastData.latitude;
                longi = Input.location.lastData.longitude;
                UpdateLocation();
            }

            // Stop service if there is no need to query location updates continuously
            Input.location.Stop();
    
}

    void UpdateLocation() {
        LocalizationDataAPI.LatitudeNow = lat;
        LocalizationDataAPI.LongitudeNow = longi;
        LocalizationDataAPI.UpdateGPSList();
    }
}
