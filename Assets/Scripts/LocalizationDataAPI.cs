﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public static class LocalizationDataAPI
    {
        private static string choosenCity;
        private static double longitudeNow = -8.58;
        private static double latitudeNow = 41.17 ;
        private static Boolean autoLocalization = false;
        private static string cityNameError = "Station temporarily unavailable";

        private static List<string> cities = new List<string>()
        {
            "Warszawa",
            "Delhi",
            "Beijing",
            "Shanghai",
            "Sofia",
            "Skopje",
            "Rome",
            "Bangkok",
            "Mexico",
            "New York", //New%20York
            "Bogota",
            "Los Angeles",//Los%20Angeles
            "Sydney",
            "Vancouver", //tu moze byc dziwnie 
            "Berlin",
            "Ethiopia",
            "Jerusalem",
            "London",
            "Kalkuta",
            "Madrid",
            "Dublin",
        };

        private static List<string> citiesAround = new List<string>();

        public static string ChoosenCity { get => choosenCity; set => choosenCity = value; }
        public static double LongitudeNow { get => longitudeNow; set => longitudeNow = value; }
        public static double LatitudeNow { get => latitudeNow; set => latitudeNow = value; }
        public static bool AutoLocalization { get => autoLocalization; set => autoLocalization = value; }
        public static string CityNameError { get => cityNameError; set => cityNameError = value; }

        static LocalizationDataAPI()
        {
           var  api = new APIPollutions("here");
           LocalizationDataAPI.ChoosenCity = api.CityPollution.CityName;
           LocalizationDataAPI.latitudeNow = Convert.ToDouble(api.CityPollution.GeoLat);
           LocalizationDataAPI.longitudeNow = Convert.ToDouble(api.CityPollution.GeoLon);

            UpdateGPSList();
        }

        static class StationsAround
        {
          
             static double addsLonDegree = 1; //y
            static double addLatDegree = 3; //x

            static public List<string> getCititesAround()
            {
                List<string> stationsAround = new List<string>();
                double newLat1 = LatitudeNow;
                double newLong1 = LongitudeNow;

                double newLat2 = LatitudeNow;
                double newLong2 = LongitudeNow;


                //A: x + 2 , y  | B: x - 2, y
                newLat1 += addLatDegree;
                newLat2 -= addLatDegree;
                APIPollutions api = new APIPollutions(newLat1, newLong1 , newLat2, newLong2);
                stationsAround = stationsAround.Union(api.cityListy).ToList();

                //A: x , y + 1 | B: x , y - 1
                newLong1 += addsLonDegree;
                newLong2 -= addsLonDegree;
                api = new APIPollutions(newLat1, newLong1, newLat2, newLong2);
                stationsAround = stationsAround.Union(api.cityListy).ToList();

                //A: x - 2 , y  | B: x + 2, y 
                newLat1 -= addLatDegree;
                newLat2 += addLatDegree;
                api = new APIPollutions(newLat1, newLong1, newLat2, newLong2);
                stationsAround = stationsAround.Union(api.cityListy).ToList();

                //A: x - 2 , y  | B: x + 2, y 
                newLat1 -= addLatDegree;
                newLat2 += addLatDegree;
                api = new APIPollutions(newLat1, newLong1, newLat2, newLong2);
                stationsAround = stationsAround.Union(api.cityListy).ToList();

                
                return stationsAround;
            }
        }

       public static void UpdateGPSList()
        {
            citiesAround = StationsAround.getCititesAround();
            cities = cities.Union(citiesAround).ToList();
        }

        public static List<string> getCitiesList()
        {
            return cities;
        }
    }
}
